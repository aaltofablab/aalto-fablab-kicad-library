# Aalto Fablab KiCad Library

This library contains symbols and footprints for all the parts that are not part of the [Fab](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad) library. Use both for maximum performance.

## Usage

First, clone or download this repository, then, open KiCad.

**Set up symbol library**

1. In project manager, go to Preferences > Manage Symbol Libraries
1. Click on the folder icon (next to + icon) and navigate to Aalto Fablab library location on your disk
1. Select the AaltoFablab.kicad_sym file and hit OK
1. Confirm and close the Manage Symbol Libraries Modal

**Set up footprint library**

1. In project manager, go to Preferences > Manage Footprint Libraries
1. Click on the folder icon (next to + icon) and navigate to Aalto Fablab library location on your disk
1. Select the AaaltoFablab.pretty folder and hit OK
1. Confirm and close the Manage Footprint Libraries modal

**For 3D models to work**

1. In project manager, go to Preferences > Configure Paths...
1. Click on the + icon and enter `AALTO_FABLAB` in the lefthand side field
1. Click into the righthand field, a folder icon should appear
1. Click the folder icon and navigate to Aalto Fablab library location on your disk
1. Confirm and close the Configure Paths... modal

## Contributing

There are several reasons why you might want to contribute.

1. New parts appear from nowhere all the time at the Aalto Fablab and everybody saves design time if those are available in the KiCad library.
1. You do not want to slow down your process by using the vast library that comes with KiCad. You prefer to have a small library with just the parts available at the lab.
1. You want to learn more about electronics, KiCad and computer-aided design.

The general flow of contributing new parts is as follws.

1. Idendify the part at the lab and check if it exists in the Aalto Fablab KiCad library. Ask around if somebody is or has been working on adding the part to the library.
1. Fork this repository and clone it to your computer. 
1. Create a new branch for adding the new component with `git checkout -b add_manufacturer_partnumber`. Replace the `manufacturer_partnumber` with the manufacturer name and number accordingly. For example `add_tracopower_tsr1`.
1. In KiCad, open the Symbol editor and create a new symbol. Check the [KiCad Library Convention (KLC)](https://klc.kicad.org/) to do it properly. 
1. Name the symbol according to this pattern `Type_Parameters_Manufacturer_ProductNumber_Footprint`. For example `Regulator_Switching_5V_1A_TracoPower_TSR1`. Every component type might have subtypes. Create an issue if not sure.
1. Create footprint and 3D model with the same name.
1. Run tests in the `tests` directory before commiting. Use `./tests/symbols.sh` for
1. Create a commit and push the new branch to your forked repository.
1. Go to your GitLab account repository and submit a merge/pull request. We will review your contribution and add it to the mainline.

The guide above is the first version of it so if you have questions, please open an issue.
